#include "map.hpp"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

Map::Map(){
  ifstream f1("doc/map.txt");
  string line;
  if(!f1){
    cout<<"Erro ao abrir o arquivo";
  }
  for(int i = 0; i < 13; i++){
    for(int j = 0; j < 13; j++){
        f1 >> arr[i][j];
    }
  }

}

Map::Map(string address){
    ifstream f1(address);
    string line;
    if(!f1){
      cout<<"Erro ao abrir o arquivo";
    }
    for(int i = 0; i < 13; i++){
      for(int j = 0; j < 13; j++){
          f1 >> arr[i][j];
      }
    }
}

void Map::draw(){
    cout<<"|___|__A__|__B__|__C__|__D__|__E__|__F__|__G__|__H__|__I__|__J__|__K__|__L__|__M__|\n";
    for(int i=0; i<13; i++){
      if(i<9){
        cout<<"|"<<i+1<<"  |";
      }else{
        cout<<"|"<<i+1<<" |";
      }
      for(int j=0; j<13; j++){
        if(shots[i][j] != 0){
          cout<<"  "<<arr[i][j]<<"  |";
        }else{
          cout<<"  -  |";
        }
      }
      cout<<"\n";
    }
    cout<<"Legenda: "<<endl;
    cout<<"-: espaço não recebeu tiro"<<endl;
    cout<<"0: água"<<endl;
    cout<<"1: barco"<<endl;
    cout<<"2: submarino"<<endl;
    cout<<"3: porta aviões"<<endl;
}

void Map::shoot(int x, int y){
    cout << "\x1B[2J\x1B[H";
    x--;
    y--;
    if(shots[x][y] == 0){
        shots[x][y] = 1;
        if(arr[x][y] == "1"){
          cout<<"Parabéns, você acertou um barco"<<endl;
        }
        if(arr[x][y] == "2"){
          cout<<"Parabéns, você acertou uma parte de submarino"<<endl;
        }
        if(arr[x][y] == "3"){
          cout<<"Parabéns, você acertou parte de um porta avião"<<endl;
        }
    }else{
        cout<<"Tiro em local inválido"<<endl;
    }
}

int Map::checkMap(){
    int flag = 1;
    for(int i=0; i<13; i++){
      for(int j=0; j<13; j++){
          if(shots[i][j] == 0 && arr[i][j] != "0"){
              flag = 0;
          }
      }
    }
    if(flag==1){
        return 1;
    }else{
        return 0;
    }
}
