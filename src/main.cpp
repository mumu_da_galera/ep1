#include <iostream>
#include <string>
#include <fstream>
#include "map.hpp"
#include "player.hpp"

using namespace std;

int main(){
    Player jogador1("doc/map.txt");
    Player jogador2("doc/map.txt");
    int x, y, turn = 1;
    while(true){
        if(turn==1){
          cout<< "Vez do JOGADOR 1"<<endl;
          jogador1.mapa->draw();
          cout << "Digite a coordenada X: ";
          cin >> x;
          cout << "Digite a coordenada Y: ";
          cin >> y;
          jogador1.mapa->shoot(x, y);
          if(jogador1.mapa->checkMap()==1){
            cout<<"Parabéns JOGADOR 1, você venceu!!!";
            break;
          }
        }else{
          cout<< "Vez do JOGADOR 2"<<endl;
          jogador2.mapa->draw();
          cout << "Digite a coordenada X: ";
          cin >> x;
          cout << "Digite a coordenada Y: ";
          cin >> y;
          jogador2.mapa->shoot(x, y);
          if(jogador2.mapa->checkMap()==1){
            cout<<"Parabéns JOGADOR 2, você venceu!!!";
            break;
          }
        }
        turn = turn*-1;
    }
    return 0;
}
