#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "map.hpp"

class Player{
  private:
    int points;
  public:
    Player(string address);
    ~Player();
    Map *mapa;
};
#endif
