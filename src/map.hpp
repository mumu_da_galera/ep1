#ifndef MAP_HPP
#define MAP_HPP

#include <string>

using namespace std;

class Map{
  private:
    // 0 = invisível, 1 = água, 2 = barco, 3 = sub, 4 = porta aviao
    string arr[13][13];
    int shots[13][13] = {};
  public:
    Map();
    Map(string address);
    void shoot(int x, int y);
    void draw();
    int checkMap();
};
#endif
